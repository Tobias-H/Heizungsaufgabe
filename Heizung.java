
/**
 * Beschreiben Sie hier die Klasse Heizung.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Heizung
{
    private double temperatur;
    private double minTemperatur;
    private double maxTemperatur;
    private double schrittweite;

    /**
     * Konstruktor f�r Objekte der Klasse Heizung
     */
    public Heizung(double min, double max)
    {
        temperatur = 15.0;
        minTemperatur = min;
        maxTemperatur = max;
        schrittweite = 5;
    }

    /**
     * Erh�ht die Temperatur um 5 Grad.
     */
    public void waermer(){
        if((temperatur + schrittweite) <= maxTemperatur){
            temperatur += schrittweite;
        } else {
            temperatur = maxTemperatur;
            System.out.println("Maximalwert erreicht");
        }
    }
    
    /**
     * Verringert  die Temperatur um 5 Grad.
     */
    public void kuehler(){
        if((temperatur - schrittweite) >= minTemperatur){
            temperatur -= schrittweite;
        } else {
            temperatur = minTemperatur;
            System.out.println("Minimalwert erreicht");
        }
    }
    
    /**
     * getter von temperatur
     */
    public double gibTemperatur(){
        return temperatur;
    }
    
    /**
     * setter von schrittweite
     */
    public void setzteSchrittweite(int weite){
        if(weite > 0){
            schrittweite = weite;
        } else {
            System.out.println("Die Schrittweite muss positiv sein");
        }
    }
}
